package Raumschiffe;

public class Ladung {

    private int menge;
    private String bezeichnung;
    
    public Ladung() {
    	
	}
    public Ladung(String bezeichnung, int menge) {
        this.bezeichnung = bezeichnung;
        this.setMenge(menge);
    }
    
    public String getBezeichnung() {
        return bezeichnung;
    }
    
    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }
    
    public int getMenge() {
        return menge;
    }
    
    public void setMenge(int menge) {
        this.menge = menge;
        if(this.menge < 1) {
            this.menge = 1;
        }
    }
}