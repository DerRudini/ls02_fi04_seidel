package kreis;

public class Rechteck {

    private double x;
    private double y;
    
    private double width;
    private double height;
    
    public Rechteck(double x, double y, double width, double height) {
        this.setX(x);
        this.setY(y);
        
        this.setWidth(width);
        this.setHeight(height);
    }
    
    public double getX() {
        return x;
    }
    
    public void setX(double x) {
        this.x = x;
        if(this.x < 0) {
            this.x = 0;
        }
    }
    
    public double getY() {
        return y;
    }
    
    public void setY(double y) {
        this.y = y;
        if(this.y < 0) {
            this.y = 0;
        }
    }
    
    public double getWidth() {
        return width;
    }
    
    public void setWidth(double width) {
        this.width = width;
        if(this.width < 0) {
            this.width = 0;
        }
    }
    
    public double getHeight() {
        return height;
    }
    
    public void setHeight(double height) {
        this.height = height;
        if(this.height < 0) {
            this.height = 0;
        }
    }
    
    public double getFlaeche() {
        return this.width * this.height;
    }
    
    public double getUmfang() {
        return (2 *this.width) + (2 *this.height);
    }
}
