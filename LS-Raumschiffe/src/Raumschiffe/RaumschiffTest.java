package Raumschiffe;

public class RaumschiffTest {

    public static void main(String[] args) {
        // Klingonen
        Raumschiff klingonen = new Raumschiff("IKS Hegh'ta", 100, 100, 100, 100, 1, 2);
        Ladung schneckensaft = new Ladung("Ferengi Schneckensaft", 200);
        Ladung batleth = new Ladung("Bat'leth Klingen Schwert", 200);
        
        klingonen.addLadung(schneckensaft);
        klingonen.addLadung(batleth);
        
        // Romulaner
        Raumschiff romulaner = new Raumschiff("IRW Khazara", 100, 100, 100, 100, 2, 2);
        Ladung borgSchrott = new Ladung("Borg-Schrott", 5);
        Ladung plasmaWaffe = new Ladung("Plasma-Waffe", 50);
        Ladung roteMaterie = new Ladung("Rote Materie", 2);
        
        romulaner.addLadung(borgSchrott);
        romulaner.addLadung(plasmaWaffe);
        romulaner.addLadung(roteMaterie);
        
        // Vulkanier
        Raumschiff vulkanier = new Raumschiff("Ni'Var", 80, 50, 80, 100, 0, 5);
        Ladung forschungssonde = new Ladung("Forschungssonde", 35);
        Ladung photonentorpedo = new Ladung("Photonentorpedo", 3);
        
        vulkanier.addLadung(forschungssonde);
        vulkanier.addLadung(photonentorpedo);
        
        // Funktion
        klingonen.photonentorpedosAbschießen(romulaner);
        romulaner.phaserkanonenAbschießen(klingonen);
        vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
        klingonen.zustandAusgabe();
        klingonen.ladungsverzeichnisAusgeben();
//        vulkanier.androidenAussetzen();
//        vulkanier.ladungVerladen();
        klingonen.photonentorpedosAbschießen(romulaner);
        klingonen.photonentorpedosAbschießen(romulaner);
        klingonen.zustandAusgabe();
        klingonen.ladungsverzeichnisAusgeben();
        klingonen.zustandAusgabe();
        romulaner.ladungsverzeichnisAusgeben();
        klingonen.zustandAusgabe();
        vulkanier.ladungsverzeichnisAusgeben();
    }
}
