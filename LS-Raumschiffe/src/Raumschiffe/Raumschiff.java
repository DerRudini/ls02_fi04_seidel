/**
 * @author chris seidel
 * @version 1.0
 */
package Raumschiffe;

import java.util.ArrayList;

public class Raumschiff {

	private String schiffsName;
    private float schildeInProzent;
    private float huelleInProzent;
    private float energieversorgungInProzent;
    private float lebenserhaltungsystemeInProzent;
    private int photonentorpedoAnzahl;
    private int androidenAnzahl;
    public ArrayList<String> broadcastKommunikator;
	private ArrayList<Ladung> ladungen;

	/**
	 * Erstellt eine neue Instanz der Klasse Raumschiff aber ohne Parameter
	 */
    public Raumschiff() {
    	this.broadcastKommunikator = new ArrayList<String>();
		this.ladungen = new ArrayList<Ladung>();
	}
    /**
     * Erstellt neue Instanz der Klasse Raumschiff
     * @param schiffsName : Gibt Name des Schiffes an
     * @param schildeInProzent : Gibt die Schilde des Raumschiffs in Prozent an
     * @param huelleInProzent : Gibt die Huelle des Raumschiffs in Prozent an
     * @param energieversorungInProzent : Gibt die Energieversorgung des Raumschiffs in Prozent an
     * @param lebenserhaltungsystemeInProzent : Gibt die Lebenserhaltungssysteme des Raumschiffs in Prozent an
     * @param photonentorpedoAnzahl : Gibt die Anzahl der Photonentorpedos an
     * @param androidenAnzahl : Gibt die Anzahl der Androiden an Board des Raumschiffs an
     */
	public Raumschiff(String schiffsName, float schildeInProzent, float huelleInProzent,
			float energieversorungInProzent, float lebenserhaltungsystemeInProzent, int photonentorpedoAnzahl,int androidenAnzahl) {
		this.schiffsName = schiffsName;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.energieversorgungInProzent = energieversorungInProzent;
		this.lebenserhaltungsystemeInProzent = lebenserhaltungsystemeInProzent;
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.androidenAnzahl = androidenAnzahl;
		this.broadcastKommunikator = new ArrayList<String>();
		this.ladungen = new ArrayList<Ladung>();
				
	}
	/**
	 * Hier wird die Ladung dem Schiff hinzugefügt
	 * @param ladung : Ladung die dem Raumschiff hinzugefügt wird
	 */
	public void addLadung(Ladung ladung) {
		this.ladungen.add(ladung);
	}
/**
 * Gibt den Zustand des Raumschiffs in der Konsole aus 
 */
	public void zustandAusgabe() {
		System.out.println("---------- [Zustand: " + this.schiffsName + "] ----------");
        System.out.println("Schilde: " + this.schildeInProzent + "%");
        System.out.println("Huelle: " + this.huelleInProzent + "%");
        System.out.println("Energieversorgung: " + this.energieversorgungInProzent + "%");
        System.out.println("Lebenserhaltungsysteme: " + this.lebenserhaltungsystemeInProzent + "%");
        System.out.println("Anzahl der Photonentorpedos: " + this.photonentorpedoAnzahl);
        System.out.println("Anzahl der Androiden: " + this.androidenAnzahl);
	}
	/**
	 * Gibt alle Ladungen des Raumschiffs mit Menge und Anzahl in der Konsole aus
	 */
	public void ladungsverzeichnisAusgeben() {
		System.out.println("---------- [" + this.schiffsName + " - Ladungen] ----------");
		for(Ladung ladung : this.ladungen) {
			System.out.println(ladung.getBezeichnung() + " | Anzahl: " + ladung.getMenge());
		}
	}
	/**
	 * Schießt einen Photonentorpedo auf ein Ziel ab
	 * @param ziel : Gibt an auf wen die Kanone gerichtet wird
	 * if-Anweisung : Wenn es keinen Photonentorpedo gibt wird einen Nachricht an die Mitglieder des Raumschiffs gesendet
	 */
	public void photonentorpedosAbschießen(Raumschiff ziel) {
		if(this.photonentorpedoAnzahl > 0) {
			this.photonentorpedoAnzahl--;
			this.nachrichtAnAlle("Photonentorpedo abgeschossen");
			
			ziel.trefferVermerken();
		} else {
			this.nachrichtAnAlle("-=*Click*=-");
		}
	}
	/**
	 * Schießt Phaserkanonen ab und vermerkt ob getroffen wurde 
	 * @param ziel
	 * if-Anweisung : wenn die Energieversorgung unter 50% fällt wird eine Nachricht an alle Mitglieder des raumschiffs geschickt
	 */
	public void phaserkanonenAbschießen(Raumschiff ziel) {
		if(this.energieversorgungInProzent < 50) {
			this.nachrichtAnAlle("-=*Click*=-");
		} else {
			this.energieversorgungInProzent *= 0.5F;
			this.nachrichtAnAlle("Phaserkanonen abgeschossen");
			ziel.trefferVermerken();
		}
	}
	/**
	 * Sendet eine Nachricht an alle Mitglieder des Schiffes und fügt die Nachricht dem Kommunikator hinzu
	 * @param nachricht : Gibt Nachrichten aus
	 */
	public void nachrichtAnAlle(String nachricht) {
		this.broadcastKommunikator.add(nachricht);
		System.out.println(nachricht);
	}
	/**
	 * Gibt an ob das Raumschiff getroffen wurde
	 */
	private void trefferVermerken() {
		System.out.println("[" + this.schiffsName + "] wurde getroffen!");
	}
	/**
	 * Gibt den Namen des Raumschiffs an
	 * @return
	 */
	public String getSchiffsName() {
		return schiffsName;
	}
	/**
	 * Set
	 * @param 
	 */
	public void setSchiffsName(String schiffsName) {
		this.schiffsName = schiffsName;
	}
	/**
	 * Get
	 * @return 
	 */
	public float getSchildeInProzent() {
		return schildeInProzent;
	}
	/**
	 * Set
	 * @param 
	 */
	public void setSchildeInProzent(float schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}
	/**
	 * Get
	 * @return : Gibt einen Wert für huelleInProzent zurück
	 */
	public float getHuelleInProzent() {
		return huelleInProzent;
	}
	/**
	 * Set
	 * @param huelleInProzent
	 */
	public void setHuelleInProzent(float huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}
	/**
	 * Get 
	 * @return
	 */
	public float getEnergieversorungInProzent() {
		return energieversorgungInProzent;
	}
	/**
	 * Set
	 * @param energieversorungInProzent
	 */
	public void setEnergieversorungInProzent(float energieversorungInProzent) {
		this.energieversorgungInProzent = energieversorungInProzent;
	}
	/**
	 * Get
	 * @return
	 */
	public float getLebenserhaltungsystemeInProzent() {
		return lebenserhaltungsystemeInProzent;
	}
	/**
	 * Set
	 * @param lebenserhaltungsystemeInProzent
	 */
	public void setLebenserhaltungsystemeInProzent(float lebenserhaltungsystemeInProzent) {
		this.lebenserhaltungsystemeInProzent = lebenserhaltungsystemeInProzent;
	}
	/**
	 * Get
	 * @return
	 */
	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}
	/**
	 * Set
	 * @param photonentorpedoAnzahl
	 */
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}
	/**
	 * Get
	 * @return
	 */
	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}
	/**
	 * Set
	 * @param androidenAnzahl
	 */
	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}
	/**
	 * Get
	 * @return
	 */
	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}
	/**
	 * Set
	 * @param broadcastKommunikator
	 */
	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	} 
	/**
	 * Fügt Ladung hinzu
	 */
	public void AddLadung() {
		
	}
}
